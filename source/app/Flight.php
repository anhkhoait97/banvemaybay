<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flights';
        protected $fillable = [
	    'id',
        'seat_id',
        'schedule_id',
        'status',
	];
}
