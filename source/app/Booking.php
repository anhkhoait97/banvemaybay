<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'bookings';
        protected $fillable = [
	    'id',
	    'client_id',
        'schedule_id',
        'aircraft_id',
        'seat_id',
        'price',

	];
}
