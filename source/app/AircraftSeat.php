<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AircraftSeat extends Model
{
    protected $table = 'aircraft_seats';
        protected $fillable = [
	    'id',
	    'name',
        'aircraft_id',
        'status'
	];
}
