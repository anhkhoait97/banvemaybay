<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $table = 'aircrafts';
        protected $fillable = [
	    'id',
	    'name',
	    'airline_id'
	];
}
