<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $table = 'airports';
        protected $fillable = [
	    'id',
	    'name',
        'address',
        'description',
        'location'
	];
}
