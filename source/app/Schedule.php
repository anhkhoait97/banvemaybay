<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'post';
        protected $fillable = [
	    'id',
        'aircraft_id',
        'datestart',
        'dateend',
        'timestart',
        'timend',
        'orginallocation',
        'destinationlocation',
        'price'
	];
}
