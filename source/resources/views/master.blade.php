<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">        
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="vi" xml:lang="vi">
<head>
    @include('layouts.header')
    @include('layouts.app')

</head>
<body style="background:#fff url('https://jetstarair.vn/image/') no-repeat center  top">
    @yield('content')
    @include('layouts.script')
    @include('layouts.footer')
</body>
</html>