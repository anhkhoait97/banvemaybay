<div id="container-footer">
<div class="top-footer-menu">
   <div class="menu-footer">
    <div class="column">
     <div>
                    <h3>Về chúng tôi</h3>
            <ul>

              
                
                  <li><a href="https://jetstarair.vn/dieu-khoan-su-dung"><span>Điều khoản sử dụng</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/huong-dan-chuyen-khoan"><span>Hướng dẫn chuyển khoản</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/cau-hoi-thuong-gap"><span>Câu hỏi thường gặp </span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/phuong-thuc-dat-ve"><span>Phương thức đặt vé</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/ve-chung-toi"><span>Giới thiệu về Jetstar Việt Nam</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/phuong-thuc-thanh-toan"><span>Phương thức  thanh toán</span></a></li>   

                  

              
              <li><a href="https://jetstarair.vn/lien-he"><span>Liên hệ với chúng tôi</span></a></li>

            </ul>

         

     </div>
     <div>
                   <h3>Chính sách</h3>
            <ul>

              
                
                  <li><a href="https://jetstarair.vn/chinh-sach/bao-mat-thong-tin"><span>Bảo mật thông tin</span></a></li>   

                  
               
              
                
                  <li><a href="https://jetstarair.vn/chinh-sach/chinh-sach-van-chuyen-hanh-khach.html"><span>Chính sách vận chuyển hành khách</span></a></li>   

                  
               
              
                
                  <li><a href="https://jetstarair.vn/chinh-sach/chinh-sach-doi-tra-va-hoan-tien"><span>Chính sách đổi trả và hoàn tiền</span></a></li>   

                  
               
              
                
                  <li><a href="https://jetstarair.vn/chinh-sach/chinh-sach-quy-dinh-chung.html"><span>Chính sách &amp; Quy định chung</span></a></li>   

                  
               
                          
            </ul>

         
     </div>
     
     <div>
                    <h3>Thông tin cần biết</h3>
            <ul>

              
                
                  <li><a href="https://jetstarair.vn/cach-tinh-cuoc-phi-hanh-ly-ky-gui-cua-jetstar-pacific.html"><span>Cách tính cước phí hành lý ký gửi của Jetstar Pacific</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/hoan-doi-ve-jetstar.html"><span>Hoàn đổi vé Jetstar</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/nhung-thong-tin-can-biet-khi-di-may-bay.html"><span>Những thông tin cần biết khi đi máy bay</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/quy-dinh-hanh-ly-cua-jetstar.html"><span>Quy định hành lý của Jetstar </span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/tre-em-di-may-bay-mot-minh-tren-cac-chuyen-bay-cua-jetstar.html"><span>Trẻ em đi máy bay một mình trên các chuyến bay của Jetstar</span></a></li>   

                  

              
                
                  <li><a href="https://jetstarair.vn/dieu-kien-ve-jetstar.html"><span>Điều kiện vé Jetstar</span></a></li>   

                  

              
            </ul>

         
     </div>
     <!-- <div class="footer-module-last">
                        <img src="image/goi-vao-vp.png">
                       <p> Tổng đài đặt vé<br>
                       <span>0947899993</span></p>
					  
<img src="http://online.gov.vn/PublicImages/2015/08/27/11/20150827110756-dathongbao.png" class="brand-logo">
     </div> -->
  </div>
 </div>
 </div>
 <div id="footer">
    <div class="wrap">
        <div class="left">
              <div class="address">
                  <h3>JetstarAir.vn - Vé máy bay Jetstar Pacific</h3>
                  <p>Mã Số Thuế: 0313870837</p>
                  <p>Công Ty TNHH PN Việt Nam<br />
8, Trần Quốc Tuấn, Phường 1, Quận Gò Vấp, Thành Phố Hồ Chí Minh, Việt Nam</p>
                  <p>Hotline: 0947899993</p>
                  
                  <p>Email: hotro@jetstarair.vn</p>
                  <p>Website: https://jetstarair.vn/</p>
                  
                <p>
                     <a href="https://jetstarair.vn/" title="https://jetstarair.vn/">2016 coppyright @<b> https://jetstarair.vn/</b></a>
                </p>
              </div>          
             
        </div>  
        <div class="right_footer">
		<div class="fb-page" data-href="https://www.facebook.com/vnjetstarair" data-tabs="timeline" data-width="450" data-height="150" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/vnjetstarair" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/vnjetstarair">JetstarAir</a></blockquote></div>	
		</div>          
        <div style="clear:both;"></div>   



         </div>
         <div class="outshow">
            
        </div>
        <div class="inshow">
            
    </div>
     <div id="loading-div-background" style="display:none;">
        <div id="loading-div" class="ui-corner-all" >
        <img src="https://jetstarair.vn/image/WaitingBig.gif" style="height:15px;margin:30px;" />        
         </div>
     </div>
<div class="sanbay" style="display:none">   
  <ul class="tabCity" id="tabs_listCity-arrival">
    <li><a class="selected" id="sbquocnoi"  style="">NỘI ĐỊA</a></li>
    <li><a id="sbquocte" style="" class="">QUỐC TẾ</a></li>
  </ul>
    
  <div class="internation-city">
            
            <input id="airport" class="inter-city txtFlightCity ac_input" datatype="arrival" datavalue="" style="font-size: 13px" name="inter-city-arrival" type="text" placeholder="Nhập tên thành phố hoặc sân bay" value="" autocomplete="off">
            
  </div>
    
  <div class="quocnoi" id="sbquocnoi">   
  <ul class="alignleft">
		<li class="title">Miền Bắc</li>
         <li class="aircode"><a code="HAN" typeflight="0" airportcode="8772,Hà Nội (HAN)" ><b>Hà Nội </b><span>(HAN)</span></a></li>
         <li class="aircode"><a code="HPH" typeflight="0" airportcode="8758,Hải Phòng (HPH)" ><b>Hải Phòng </b><span>(HPH)</span></a></li>
         <li class="aircode"><a code="DIN" typeflight="0" airportcode="8762,Điện Biên Phủ (DIN)" ><b>Điện Biên </b><span>(DIN)</span></a></li>
         
  </ul>                   
     
    <ul class="alignleft" id="alightcenter">       
           <li class="title"> Miền Trung</li> 
         <li class="aircode"><a code="THD" typeflight="0" airportcode="8906,Thanh Hóa (THD)" ><b>Thanh Hóa </b><span>(THD)</span></a></li>
         <li class="aircode"><a code="VII" typeflight="0" airportcode="8786,Vinh  (VII)" ><b>Vinh  </b><span>(VII)</span></a></li>
         <li class="aircode"><a code="DAD" typeflight="0" airportcode="8761,Đà Nẵng (DAD)" ><b>Đà Nẵng </b><span>(DAD)</span></a></li>
        <li class="aircode"><a code="HUI" typeflight="0" airportcode="8775,Huế (HUI)" ><b>Huế </b><span>(HUI)</span></a></li>
        <li class="aircode"><a code="VDH" typeflight="0" airportcode="8902,Đồng Hới (VDH)" ><b>Đồng Hới </b><span>(VDH)</span></a></li>
        <li class="aircode"><a code="TBB" typeflight="0" airportcode="8763,Tuy Hòa (TBB)" ><b>Tuy Hòa </b><span>(TBB)</span></a></li>
        <li class="aircode"><a code="VCL" typeflight="0" airportcode="8903,Chu Lai (VCL)" ><b>Chu Lai </b><span>(VCL)</span></a></li>
           <li class="aircode"><a code="UIH" typeflight="0" airportcode="8781,Quy Nhơn (UIH)" ><b>Quy Nhơn </b><span>(UIH)</span></a></li>    
        
   </ul>     
<ul class="alignleft" id="alightcenter">       
       <li class="title">Tây Nguyên</li> 
        <li class="aircode"><a code="PXU" typeflight="0" airportcode="8779,Pleiku (PXU)" ><b>Pleiku </b><span>(PXU)</span></a></li>
        <li class="aircode"><a code="DLI" typeflight="0" airportcode="8766,Đà Lạt (DLI)" ><b>Đà Lạt </b><span>(DLI)</span></a></li>
        <li class="aircode"><a code="BMV" typeflight="0" airportcode="8777,Buôn Ma Thuột (BMV)" ><b>Buôn Ma Thuột </b><span>(BMV)</span></a></li>
           
   </ul>      
   <ul class="alignleft" >
            <li class="title">Miền Nam</li>
            <li class="aircode"><a code="SGN" typeflight="0" airportcode="8785,Hồ Chí Minh (SGN)" ><b>Hồ Chí Minh </b><span>(SGN)</span></a></li>
            <li class="aircode"><a code="CXR" typeflight="0" airportcode="8769,Nha Trang (CXR)" ><b>Nha Trang </b><span>(CXR)</span></a></li>
              <li class="aircode"><a code="PQC" typeflight="0" airportcode="8764,Phú Quốc (PQC)" ><b>Phú Quốc </b><span>(PQC)</span></a></li>
            <li class="aircode"><a code="VCA" typeflight="0" airportcode="8756,Cần Thơ (VCA)" ><b>Cần Thơ </b><span>(VCA)</span></a></li>
            <li class="aircode"><a code="VCS" typeflight="0" airportcode="8760,Côn Đảo (VCS)" ><b>Côn Đảo </b><span>(VCS)</span></a></li>
            <li class="aircode"><a code="VKG" typeflight="0" airportcode="8782,Rạch Giá (VKG)" ><b>Rạch Giá </b><span>(VKG)</span></a></li>
            <li class="aircode"><a code="CAH" typeflight="0" airportcode="8755,Cà Mau (CAH)" ><b>Cà Mau </b><span>(CAH)</span></a></li>
         
          
    </ul>          
   
 </div><!--Quoc noi-->
 
 <div id="sbquocte">
 <div class="domestic-col">
       <ul>
             <li class="title">Đông Nam Á</li>
             <li class="aircode" code="HAN" typeflight="0"><a ><img src="https://jetstarair.vn/image/flag/VN.png" width="22px" height="15px" align="absmiddle" alt="Hà Nội">&nbsp;<b>Hà Nội </b><span>&nbsp;(HAN)</span></a></li>
             <li code="SGN"  class="aircode" typeflight="0"><a ><img src="https://jetstarair.vn/image/flag/VN.png" width="22px" height="15px" align="absmiddle" alt="Hồ Chí Minh">&nbsp;<b>Hồ Chí Minh </b><span>&nbsp;(SGN)</span></a></li>
             <li class="aircode" code="SIN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/SG.png" width="22px" height="15px" align="absmiddle" alt="Singapore">&nbsp;<b>Singapore </b><span>&nbsp;(SIN)</span></a></li>
             <li class="aircode" code="BKK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TH.png" width="22px" height="15px" align="absmiddle" alt="Bangkok">&nbsp;<b>Bangkok </b><span>&nbsp;(BKK)</span></a></li>
             <li class="aircode" code="PNH" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KH.png" width="22px" height="15px" align="absmiddle" alt="Phnom Penh">&nbsp;<b>Phnom Penh </b><span>&nbsp;(PNH)</span></a></li>
             <li class="aircode" code="REP" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KH.png" width="22px" height="15px" align="absmiddle" alt="Siem Reap">&nbsp;<b>Siem Reap </b><span>&nbsp;(REP)</span></a></li>
            <li class="aircode" code="CGK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/ID.png" width="22px" height="15px" align="absmiddle" alt="Jakarta">&nbsp;<b>Jakarta </b><span>&nbsp;(CGK)</span></a></li>
            <li class="aircode" code="MNL" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/PH.png" width="22px" height="15px" align="absmiddle" alt="Manila">&nbsp;<b>Manila </b><span>&nbsp;(MNL)</span></a></li>
            <li class="aircode" code="KUL" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Kuala Lumpur">&nbsp;<b>Kuala Lumpur </b><span>&nbsp;(KUL)</span></a></li>
			<li class="aircode" code="PEN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Penang">&nbsp;<b>Penang </b><span>&nbsp;(PEN)</span></a></li>
			<li class="aircode" code="JHB" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Johor Bahru">&nbsp;<b>Johor Bahru </b><span>&nbsp;(JHB)</span></a></li>
			<li class="aircode" code="RGN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MM.png" width="22px" height="15px" align="absmiddle" alt="Yangon"><b>Yangon </b><span>&nbsp;(RGN)</span></a></li>
 			<li class="aircode" code="VTE" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/LA.png" width="22px" height="15px" align="absmiddle" alt="Vientiane">&nbsp;<b>Vientiane </b><span>&nbsp;(VTE)</span></a></li>
             </ul>
     </div>
     <div class="domestic-col">
        <ul>
           <li class="title">Trung Quốc</li>
            <li class="aircode" code="HKG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/HK.png" width="22px" height="15px" align="absmiddle" alt="Hong Kong">&nbsp;<b>Hong Kong </b><span>&nbsp;(HKG)</span></a></li>
            <li class="aircode" code="MFM" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MO.png" width="22px" height="15px" align="absmiddle" alt="Macau">&nbsp;<b>Macau </b><span>&nbsp;(MFM)</span></a></li>
    
            <li class="aircode" code="CAN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Quảng Châu">&nbsp;<b>Quảng Châu </b><span>&nbsp;(CAN)</span></a></li>
            <li class="aircode" code="PEK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Bắc Kinh">&nbsp;<b>Bắc Kinh </b><span>&nbsp;(PEK)</span></a></li>
            <li class="aircode" code="CSX" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Changsha">&nbsp;<b>Changsha </b><span>&nbsp;(CSX)</span></a></li>
            <li class="aircode" code="SHA" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Thượng Hải">&nbsp;<b>Thượng Hải </b><span>&nbsp;(SHA)</span></a></li>
            <li class="aircode" code="NNG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Nam Ninh">&nbsp;<b>Nam Ninh </b><span>&nbsp;(NNG)</span></a></li>
            <li class="aircode" code="PVG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Thượng Hải">&nbsp;<b>Thượng Hải </b><span>&nbsp;(PVG)</span></a></li>
            </ul></div>
		<div class="domestic-col"><ul>
			<li class="title">Bắc Á</li>
		    <li class="aircode" code="NGO" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Nagoya">&nbsp;<b>Nagoya </b><span>&nbsp;(NGO)</span></a></li>
            <li class="aircode" code="KIX" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Osaka">&nbsp;<b>Osaka </b><span>&nbsp;(KIX)</span></a></li>
          
            <li class="aircode" code="NRT" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Tokyo">&nbsp;<b>Tokyo </b><span>&nbsp;(NRT)</span></a></li>
            <li class="aircode" code="PUS" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Busan">&nbsp;<b>Busan </b><span>&nbsp;(PUS)</span></a></li>
            <li class="aircode" code="GMP" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Seoul">&nbsp;<b>Seoul </b><span>&nbsp;(GMP)</span></a></li>
            <li class="aircode" code="ICN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Seoul">&nbsp;<b>Seoul </b><span>&nbsp;(ICN)</span></a></li>
            <li class="aircode" code="KHH" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Cao Hùng">&nbsp;<b>Cao Hùng </b><span>&nbsp;(KHH)</span></a></li>
            <li class="aircode" code="TPE" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Đài Bắc">&nbsp;<b>Đài Bắc </b><span>&nbsp;(TPE)</span></a></li>
			<li class="aircode" code="TNN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Đài Nam">&nbsp;<b>Đài Nam </b><span>&nbsp;(TNN)</span></a></li>
			  <li class="title">Châu Úc</li>
            <li class="aircode" code="PER" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Perth">&nbsp;<b>Perth </b><span>&nbsp;(PER)</span></a></li>
            <li class="aircode" code="SYD" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Sydney">&nbsp;<b>Sydney </b><span>&nbsp;(SYD)</span></a></li>
            <li class="aircode" code="MEL" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Melbourne">&nbsp;<b>Melbourne </b><span>&nbsp;(MEL)</span></a></li>
			<li class="aircode" code="ADL" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Adelaide">&nbsp;<b>Adelaide </b><span>&nbsp;(ADL)</span></a></li>
	
		</ul></div>
		<div class="domestic-col"><ul>
			<li class="title">Châu Âu</li>
            <li class="aircode" code="PRG" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/CZ.png" width="22px" height="15px" align="absmiddle" alt="Prague">&nbsp;<b>Prague </b><span>&nbsp;(PRG)</span></a></li>
            <li class="aircode" code="CDG" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/FR.png" width="22px" height="15px" align="absmiddle" alt="Paris">&nbsp;<b>Paris </b><span>&nbsp;(CDG)</span></a></li>
            <li class="aircode" code="FRA" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/DE.png" width="22px" height="15px" align="absmiddle" alt="Frankfurt">&nbsp;<b>Frankfurt </b><span>&nbsp;(FRA)</span></a></li>
            <li class="aircode" code="MOW" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/RU.png" width="22px" height="15px" align="absmiddle" alt="Moscow">&nbsp;<b>Moscow </b><span>&nbsp;(MOW)</span></a></li>
            <li class="aircode" code="LHR" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/CA.png" width="22px" height="15px" align="absmiddle" alt="London">&nbsp;<b>London </b><span>&nbsp;(LHR)</span></a></li>
			         <li class="title">Châu Mỹ</li>
            <li class="aircode" code="YTO" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/CA.png" width="22px" height="15px" align="absmiddle" alt="Toronto">&nbsp;<b>Toronto </b><span>&nbsp;(YTO)</span></a></li>
            <li class="aircode" code="CHI" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Chicago">&nbsp;<b>Chicago </b><span>&nbsp;(CHI)</span></a></li>
            <li class="aircode" code="BOS" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Boston">&nbsp;<b>Boston </b><span>&nbsp;(BOS)</span></a></li>
            <li class="aircode" code="MSP" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Minneapolis">&nbsp;<b>Minneapolis </b><span>&nbsp;(MSP)</span></a></li>
            <li class="aircode" code="SFO" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="San Francisco">&nbsp;<b>San Francisco </b><span>&nbsp;(SFO)</span></a></li>
            <li class="aircode" code="SEA" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Seattle, WA">&nbsp;<b>Seattle, WA </b><span>&nbsp;(SEA)</span></a></li>
            <li class="aircode" code="LAX" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/CL.png" width="22px" height="15px" align="absmiddle" alt="Los Angeles">&nbsp;<b>Los Angeles </b><span>&nbsp;(LAX)</span></a></li>
		</ul>
     </div>  
  </div> 
</div><!--end tab-->  
 
 
<div class="sanbayres" style="display:none">
  <ul class="tabCity" id="tabs_listCity-arrival">
    <li><a  class="selected" id="sbquocnoi"  style="">NỘI ĐỊA</a></li>
    <li><a id="sbquocte" style="" class="">QUỐC TẾ</a></li>
  </ul>
    <div class="internation-city">
            
            <input id="airportres" class="inter-city txtFlightCity ac_input" datatype="arrival" datavalue="" style="font-size: 13px" name="inter-city-arrival" type="text" placeholder="Nhập tên thành phố hoặc sân bay" value="" autocomplete="off">
            
    </div>
    
    <div class="quocnoi" id="sbquocnoi"> 
            <ul class="alignleft">
				<li class="title">Miền Bắc</li>
                 <li class="aircode"><a code="HAN" typeflight="0" airportcode="8772,Hà Nội (HAN)" ><b>Hà Nội </b><span>(HAN)</span></a></li>
                 <li class="aircode"><a code="HPH" typeflight="0" airportcode="8758,Hải Phòng (HPH)" ><b>Hải Phòng </b><span>(HPH)</span></a></li>
                 <li class="aircode"><a code="DIN" typeflight="0" airportcode="8762,Điện Biên Phủ (DIN)" ><b>Điện Biên </b><span>(DIN)</span></a></li>
        </ul>              
        <ul class="alignleft" id="alightcenter">       
				<li class="title">Miền Trung</li>
                <li class="aircode"><a code="THD" typeflight="0" airportcode="8906,Thanh Hóa (THD)" ><b>Thanh Hóa </b><span>(THD)</span></a></li>
                <li class="aircode"><a code="VII" typeflight="0" airportcode="8786,Vinh  (VII)" ><b>Vinh  </b><span>(VII)</span></a></li>
                <li class="aircode"><a code="DAD" typeflight="0" airportcode="8761,Đà Nẵng (DAD)" ><b>Đà Nẵng </b><span>(DAD)</span></a></li>
                <li class="aircode"><a code="HUI" typeflight="0" airportcode="8775,Huế (HUI)" ><b>Huế </b><span>(HUI)</span></a></li>
                <li class="aircode"><a code="VDH" typeflight="0" airportcode="8902,Đồng Hới (VDH)" ><b>Đồng Hới </b><span>(VDH)</span></a></li>
                <li class="aircode"><a code="TBB" typeflight="0" airportcode="8763,Tuy Hòa (TBB)" ><b>Tuy Hòa </b><span>(TBB)</span></a></li>
                <li class="aircode"><a code="VCL" typeflight="0" airportcode="8903,Chu Lai (VCL)" ><b>Chu Lai </b><span>(VCL)</span></a></li>
                <li class="aircode"><a code="UIH" typeflight="0" airportcode="8781,Quy Nhơn (UIH)" ><b>Quy Nhơn </b><span>(UIH)</span></a></li>
        
        </ul>       
		  <ul class="alignleft" id="alightcenter">       
				 <li class="title">Miền Nam</li> 
				<li class="aircode"><a code="PXU" typeflight="0" airportcode="8779,Pleiku (PXU)" ><b>Pleiku </b><span>(PXU)</span></a></li>
				<li class="aircode"><a code="DLI" typeflight="0" airportcode="8766,Đà Lạt (DLI)" ><b>Đà Lạt </b><span>(DLI)</span></a></li>
				<li class="aircode"><a code="BMV" typeflight="0" airportcode="8777,Buôn Ma Thuột (BMV)" ><b>Buôn Ma Thuột </b><span>(BMV)</span></a></li>
				   
		   </ul>       
                <ul class="alignleft" >
				<li class="title">Miền Nam</li>	
                <li class="aircode"><a code="SGN" typeflight="0" airportcode="8785,Hồ Chí Minh (SGN)" ><b>Hồ Chí Minh </b><span>(SGN)</span></a></li>
                <li class="aircode"><a code="CXR" typeflight="0" airportcode="8769,Nha Trang (CXR)" ><b>Nha Trang </b><span>(CXR)</span></a></li>
                <li class="aircode"><a code="PQC" typeflight="0" airportcode="8764,Phú Quốc (PQC)" ><b>Phú Quốc </b><span>(PQC)</span></a></li>
                <li class="aircode"><a code="VCA" typeflight="0" airportcode="8756,Cần Thơ (VCA)" ><b>Cần Thơ </b><span>(VCA)</span></a></li>
                <li class="aircode"><a code="VCS" typeflight="0" airportcode="8760,Côn Đảo (VCS)" ><b>Côn Đảo </b><span>(VCS)</span></a></li>
                <li class="aircode"><a code="VKG" typeflight="0" airportcode="8782,Rạch Giá (VKG)" ><b>Rạch Giá </b><span>(VKG)</span></a></li>
                <li class="aircode"><a code="CAH" typeflight="0" airportcode="8755,Cà Mau (CAH)" ><b>Cà Mau </b><span>(CAH)</span></a></li>
               
              
        </ul>               
 </div><!--Quoc noi-->
 <div id="sbquocte">
      <div class="domestic-col">
        <ul>
             <li class="title">Đông Nam Á</li>
             <li class="aircode" code="HAN" typeflight="0"><a ><img src="https://jetstarair.vn/image/flag/VN.png" width="22px" height="15px" align="absmiddle" alt="Hà Nội">&nbsp;<b>Hà Nội </b><span>&nbsp;(HAN)</span></a></li>
             <li code="SGN"  class="aircode" typeflight="0"><a ><img src="https://jetstarair.vn/image/flag/VN.png" width="22px" height="15px" align="absmiddle" alt="Hồ Chí Minh">&nbsp;<b>Hồ Chí Minh </b><span>&nbsp;(SGN)</span></a></li>
             <li class="aircode" code="SIN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/SG.png" width="22px" height="15px" align="absmiddle" alt="Singapore">&nbsp;<b>Singapore </b><span>&nbsp;(SIN)</span></a></li>
             <li class="aircode" code="BKK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TH.png" width="22px" height="15px" align="absmiddle" alt="Bangkok">&nbsp;<b>Bangkok </b><span>&nbsp;(BKK)</span></a></li>
             <li class="aircode" code="PNH" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KH.png" width="22px" height="15px" align="absmiddle" alt="Phnom Penh">&nbsp;<b>Phnom Penh </b><span>&nbsp;(PNH)</span></a></li>
             <li class="aircode" code="REP" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KH.png" width="22px" height="15px" align="absmiddle" alt="Siem Reap">&nbsp;<b>Siem Reap </b><span>&nbsp;(REP)</span></a></li>
            <li class="aircode" code="CGK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/ID.png" width="22px" height="15px" align="absmiddle" alt="Jakarta">&nbsp;<b>Jakarta </b><span>&nbsp;(CGK)</span></a></li>
            <li class="aircode" code="MNL" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/PH.png" width="22px" height="15px" align="absmiddle" alt="Manila">&nbsp;<b>Manila </b><span>&nbsp;(MNL)</span></a></li>
            <li class="aircode" code="KUL" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Kuala Lumpur">&nbsp;<b>Kuala Lumpur </b><span>&nbsp;(KUL)</span></a></li>
			<li class="aircode" code="PEN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Penang">&nbsp;<b>Penang </b><span>&nbsp;(PEN)</span></a></li>
           <li class="aircode" code="JHB" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MY.png" width="22px" height="15px" align="absmiddle" alt="Johor Bahru">&nbsp;<b>Johor Bahru </b><span>&nbsp;(JHB)</span></a></li>
           <li class="aircode" code="RGN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MM.png" width="22px" height="15px" align="absmiddle" alt="Yangon"><b>Yangon </b><span>&nbsp;(RGN)</span></a></li>
 			<li class="aircode" code="VTE" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/LA.png" width="22px" height="15px" align="absmiddle" alt="Vientiane">&nbsp;<b>Vientiane </b><span>&nbsp;(VTE)</span></a></li>
           </ul>
     </div>
     <div class="domestic-col">
        <ul>
           <li class="title">Trung Quốc</li>
            <li class="aircode" code="HKG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/HK.png" width="22px" height="15px" align="absmiddle" alt="Hong Kong">&nbsp;<b>Hong Kong </b><span>&nbsp;(HKG)</span></a></li>
            <li class="aircode" code="MFM" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/MO.png" width="22px" height="15px" align="absmiddle" alt="Macau">&nbsp;<b>Macau </b><span>&nbsp;(MFM)</span></a></li>
            
            <li class="aircode" code="CAN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Quảng Châu">&nbsp;<b>Quảng Châu </b><span>&nbsp;(CAN)</span></a></li>
            <li class="aircode" code="PEK" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Bắc Kinh">&nbsp;<b>Bắc Kinh </b><span>&nbsp;(PEK)</span></a></li>
            <li class="aircode" code="CSX" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Changsha">&nbsp;<b>Changsha </b><span>&nbsp;(CSX)</span></a></li>
            <li class="aircode" code="SHA" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Thượng Hải">&nbsp;<b>Thượng Hải </b><span>&nbsp;(SHA)</span></a></li>
            <li class="aircode" code="NNG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Nam Ninh">&nbsp;<b>Nam Ninh </b><span>&nbsp;(NNG)</span></a></li>
            <li class="aircode" code="PVG" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/CN.png" width="22px" height="15px" align="absmiddle" alt="Thượng Hải">&nbsp;<b>Thượng Hải </b><span>&nbsp;(PVG)</span></a></li>
			</ul></div>
		<div class="domestic-col"><ul>
			<li class="title">Bắc Á</li>	
		   <li class="aircode" code="NGO" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Nagoya">&nbsp;<b>Nagoya </b><span>&nbsp;(NGO)</span></a></li>
            <li class="aircode" code="KIX" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Osaka">&nbsp;<b>Osaka </b><span>&nbsp;(KIX)</span></a></li>
          
            <li class="aircode" code="NRT" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/JP.png" width="22px" height="15px" align="absmiddle" alt="Tokyo">&nbsp;<b>Tokyo </b><span>&nbsp;(NRT)</span></a></li>
            <li class="aircode" code="PUS" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Busan">&nbsp;<b>Busan </b><span>&nbsp;(PUS)</span></a></li>
            <li class="aircode" code="GMP" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Seoul">&nbsp;<b>Seoul </b><span>&nbsp;(GMP)</span></a></li>
            <li class="aircode" code="ICN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/KR.png" width="22px" height="15px" align="absmiddle" alt="Seoul">&nbsp;<b>Seoul </b><span>&nbsp;(ICN)</span></a></li>
            <li class="aircode" code="KHH" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Cao Hùng">&nbsp;<b>Cao Hùng </b><span>&nbsp;(KHH)</span></a></li>
            <li class="aircode" code="TPE" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Đài Bắc">&nbsp;<b>Đài Bắc </b><span>&nbsp;(TPE)</span></a></li>
			<li class="aircode" code="TNN" typeflight="1"><a ><img src="https://jetstarair.vn/image/flag/TW.png" width="22px" height="15px" align="absmiddle" alt="Đài Nam">&nbsp;<b>Đài Nam </b><span>&nbsp;(TNN)</span></a></li>
			<li class="title">Châu Úc</li>
            <li class="aircode" code="PER" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Perth">&nbsp;<b>Perth </b><span>&nbsp;(PER)</span></a></li>
            <li class="aircode" code="SYD" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Sydney">&nbsp;<b>Sydney </b><span>&nbsp;(SYD)</span></a></li>
            <li class="aircode" code="MEL" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Melbourne">&nbsp;<b>Melbourne </b><span>&nbsp;(MEL)</span></a></li>
			<li class="aircode" code="ADL" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/AU.png" width="22px" height="15px" align="absmiddle" alt="Adelaide">&nbsp;<b>Adelaide </b><span>&nbsp;(ADL)</span></a></li>
	
		  </ul></div><div class="domestic-col"><ul>
		 <li class="title">Châu Âu</li>
            <li class="aircode" code="PRG" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/CZ.png" width="22px" height="15px" align="absmiddle" alt="Prague">&nbsp;<b>Prague </b><span>&nbsp;(PRG)</span></a></li>
            <li class="aircode" code="CDG" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/FR.png" width="22px" height="15px" align="absmiddle" alt="Paris">&nbsp;<b>Paris </b><span>&nbsp;(CDG)</span></a></li>
            <li class="aircode" code="FRA" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/DE.png" width="22px" height="15px" align="absmiddle" alt="Frankfurt">&nbsp;<b>Frankfurt </b><span>&nbsp;(FRA)</span></a></li>
            <li class="aircode" code="MOW" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/RU.png" width="22px" height="15px" align="absmiddle" alt="Moscow">&nbsp;<b>Moscow </b><span>&nbsp;(MOW)</span></a></li>
            <li class="aircode" code="LHR" typeflight="2"><a ><img src="https://jetstarair.vn/image/flag/CA.png" width="22px" height="15px" align="absmiddle" alt="London">&nbsp;<b>London </b><span>&nbsp;(LHR)</span></a></li>
			         <li class="title">Châu Mỹ</li>
            <li class="aircode" code="YTO" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/CA.png" width="22px" height="15px" align="absmiddle" alt="Toronto">&nbsp;<b>Toronto </b><span>&nbsp;(YTO)</span></a></li>
            <li class="aircode" code="CHI" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Chicago">&nbsp;<b>Chicago </b><span>&nbsp;(CHI)</span></a></li>
            <li class="aircode" code="BOS" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Boston">&nbsp;<b>Boston </b><span>&nbsp;(BOS)</span></a></li>
            <li class="aircode" code="MSP" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Minneapolis">&nbsp;<b>Minneapolis </b><span>&nbsp;(MSP)</span></a></li>
            <li class="aircode" code="SFO" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="San Francisco">&nbsp;<b>San Francisco </b><span>&nbsp;(SFO)</span></a></li>
            <li class="aircode" code="SEA" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/US.png" width="22px" height="15px" align="absmiddle" alt="Seattle, WA">&nbsp;<b>Seattle, WA </b><span>&nbsp;(SEA)</span></a></li>
            <li class="aircode" code="LAX" typeflight="3"><a ><img src="https://jetstarair.vn/image/flag/CL.png" width="22px" height="15px" align="absmiddle" alt="Los Angeles">&nbsp;<b>Los Angeles </b><span>&nbsp;(LAX)</span></a></li>
		</ul>
     </div>   
     
 </div>    
</div><!-- end tab-->
</div><!-- End footer -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
var chuoi ="<div id='inforheadertop'><div id='menu'>";
    chuoi +=$("#menu").html();

          chuoi +="</div>";
        $(document).ready(function() {
            $('#container').append('<div id="top">Back to Top</div>');
            $('#container').append(chuoi);
                        $("#inforheadertop").hide();
            $(window).scroll(function() {
            if($(window).scrollTop() <= 120) {
              //  alert('dsg');
                $('#top').fadeOut();
                $('#inforheadertop').fadeOut();
            } else if($(window).scrollTop()>120) {
                $('#top').fadeIn();
                $('#inforheadertop').fadeIn();
            }
           });
           $('#top').click(function() {
            $('html, body').animate({scrollTop:0},500);
           });
        });    
       $(document).ready(function(){
           $('.breadcrumb_calender a:first').addClass("home");      
        });
</script>
