<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="https://fonts.googleapis.com/css?family=Maitree" rel="stylesheet">
<link href="https://jetstarair.vn/image/favicon.ico" rel="shortcut icon" />
     
<link rel="stylesheet" type="text/css" href="{{asset('res/css/reset.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('res/css/stylesheet.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('res/css/font-awesome.min.css')}}" /> 

<link rel="stylesheet" type="text/css" href="{{asset('res/css/font.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('res/css/slideshow.css')}}" media="screen" />
<!--datetime - va ui-->



<!-- Start js -->
<script type="text/javascript" src="{{asset('res/js/jquery-1.8.3.js')}}"></script>
<!--end-->
<!--chạy phần tin tức mới trên trang chủ-->
 
<script type="text/javascript" src="{{asset('res/js/jquery-1.7.1.min.js')}}"></script>
<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->          
<script type="text/javascript" src="{{asset('res/js/jquery.nivo.slider.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('res/js/jquery-ui-1.9.2.custom.js')}}"></script>
<script type="text/javascript" src="{{asset('res/js/jquery.ui.datepicker-vi.js')}}"></script>
<script type="text/javascript" src="{{asset('res/js/script.js')}}"></script>
<!--fancybox-->
<script type="text/javascript" src="{{asset('res/js/jquery.mousewheel-3.0.4.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('res/js/jquery.fancybox-1.3.4.pack.js')}}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('res/css/jquery.fancybox-1.3.4.css')}}" media="screen" />
<!--end fancybox-->
<script type="text/javascript" src="{{asset('res/js/tabs.js')}}"></script>


<!-- End -->




<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123054459-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123054459-1');
</script>
<div id="fb-root"></div>