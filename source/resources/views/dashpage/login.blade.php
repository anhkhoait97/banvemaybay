<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('dashboard/fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('dashboard/css/style1.css')}}">
</head>
<body>
    <div class="main">

        <section class="signup">
            
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form" action="">
                        <h2 class="form-title">Login</h2>
                        <div class="form-group">
                            <input type="text" class="form-input" name="email" id="user" required="" placeholder="Your Email"/>
                        </div>
                        <div class="form-group">
                            <input type="Password" class="form-input" name="pass" id="pass" placeholder="Password" required=""/>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="sub" id="sub" class="form-submit" value="Login"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Thuộc quyền sở hữu của VietTravel 
                        <!-- <a href="register.html" class="loginhere-link">Register here</a> -->
                    </p>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="{{asset('dashboard/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('dashboard/js/main.js')}}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>