@extends('master')
@section('content')
<div id="body_container">              
<div class="container-header">       
      <div id="header">
          <div class="bg_header_banner">
                  <div id="logo">
                                          <a href="https://jetstarair.vn/" title="Trang chủ">
                      <img title="Trang chủ" alt="Trang chủ" src="https://jetstarair.vn/image/data/logojet.png" /></a>
                      
                  </div>       
                  <div id="menu">
                         <ul>       
                            
                                                         
                                                                              <li class="active home"><a href="https://jetstarair.vn/" ><span>Trang chủ</span></a></li>
                                                                        
                                                                                     
                            <li class="menu90">
                              <a href="https://jetstarair.vn/huong-dan"><span>Hướng dẫn</span></a>
                                                            
                            </li>
                          
                                                                                
                            <li class="menu114">
                              <a href="https://jetstarair.vn/phuong-thuc-thanh-toan"><span>Thanh Toán</span></a>
                                                            
                            </li>
                          
                                                                                
                            <li class="menu128">
                              <a href="https://jetstarair.vn/tin-tuc"><span>Tin tức</span></a>
                                                            
                            </li>
                          
                                                                                
                            <li class="menu87">
                              <a href="https://jetstarair.vn/ve-may-bay"><span>Vé máy bay</span></a>
                                                              <ul>
                                                                     <li>
                                      <a href="https://jetstarair.vn/ve-may-bay-noi-dia"><span>Vé nội địa</span></a>
                                                                                
                                    </li>
                                 
                                                                     <li>
                                      <a href="https://jetstarair.vn/ve-may-bay-quoc-te"><span>Vé quốc tế</span></a>
                                                                                
                                    </li>
                                 
                                                                     <li>
                                      <a href="https://jetstarair.vn/ve-may-bay-theo-hang"><span>Vé theo hãng</span></a>
                                                                                
                                    </li>
                                 
                                                                     <li>
                                      <a href="https://jetstarair.vn/ve-may-bay-khuyen-mai"><span>Vé khuyến mãi</span></a>
                                                                                
                                    </li>
                                 
                                                                 </ul> 
                                                            
                            </li>
                          
                                                                                
                            <li class="menu121">
                              <a href="https://jetstarair.vn/lien-he"><span>Liên hệ</span></a>
                                                            
                            </li>
                          
                                                                        
        </ul>
              <script type="text/javascript">
                var url = window.location;
                $('#menu a').each(function() {

                if($(this).attr('href')==url) {
                        $(this).parents('li').addClass('active');
                }
                });
                $('#menu ul li:last').addClass('end');
                   
              </script>
              
                 </div>
          </div>
      </div> 
</div>      

<div id="notification"></div>
<div class="clear"></div>
<!-- js 2 -->
<script type="text/javascript" src="{{asset('res/js/ui.datepicker.lunar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('res/js/select_datetime.js')}}"></script>
    <!-- end js 2 -->
<link rel="stylesheet" type="text/css" href="{{asset('res/css/jquery-ui-1.8.7.all.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('res/css/form.css')}}" />
<div class="booking_bg">
 <div class="search_flight_home">
  <div class="form_search_home"> 
    <div class="wrapfrm"> 
            <div class="htabs_search">
                <h6 class="booking-title"><i class="fa fa-plane"></i>Tìm kiếm chuyến bay</h6>
             </div> 
             <div id="tab-search" style="display: block;">   
                        <form action="https://jetstarair.vn/tim-ve-truc-tuyens?sessionid=ac74e104eee681d5525a21e2d467db6d" method="POST" class="black" name="frmsearchair" id="frmsearchair"  >
                        
                              <div class="header">
                                     <p class="input_left">
                                        <input name="direction" id="loaive_o" value="0" onclick="disabledate(0)" type="radio"  class="loaive" checked="checked" />
                                        <input name="trip_select_hidden" id="trip_select_hidden" type="hidden" value="1"/>
                                        
                                        <label for="loaive_o">Một chiều</label>
                                     </p>      
                                     <p class="input_right">
                                        <input name="direction" id="loaive_r" onclick="disabledate(1);"  value="1"  type="radio"  class="loaive" />
                                    
                                        <label for="loaive_r">Khứ hồi</label>
                                     </p> 
                                     
                                     
                             </div>
                            <div class="first">    
                             <div class="first-content">
                                                       
                                    <div class="content">
                                        <div class="floatleft des">
                                            <label>Ðiểm đi</label>
                                            <input type="text" placeholder="Điểm đi" id="departCity" class="city cityfix" data="Lựa chọn thành phố hoặc sân bay lượt đi" name="depinput" value="" readonly="readonly"/>
                                            <input type="hidden" id="departCity_hidden" name="departCity_hidden" value="" />
                                            <i class="fa fa-crosshairs"></i>
                                        </div>
                                         <div class="floatleft res">
                                            <label>Điểm đến</label>
                                            <input type="text" id="returnCity" placeholder="Điểm đến" class="city cityfix" data="Lựa chọn thành phố hoặc sân bay lượt về" name="desinput" value="" readonly="readonly"/>
                                            <input type="hidden" id="returnCity_hidden" name="returnCity_hidden" value=""/>                             
                                            <i class="fa fa-crosshairs"></i>   
                                        </div>
                                    </div>   
                                    <div class="content">
                                          <div class="floatleft dayres">
                                            <label>Ngày đi</label>
                                             <input type="hidden"  name="txt-date-departure" class="txt-date txt-date-departure" value="" datetype="departure" />
                                            <input type="text" name="depdate" id="flights-checkin" class="date depDate " value="16-10-2018" />
                                           
                                        </div>
                                        <div class="floatleft dayres" id="ngayve">         
                                            <label>Ngày về</label>          
                                            <input type="hidden" name="txt-date-return" class="txt-date txt-date-return" value="" datetype="return" />
                                            <input type="text" name="resdate"  id="flights-checkout" class="date retDate" value="18-10-2018" />                                    
                                        </div>
                                    </div>
                                   <div class="content">   
                                        <div class="floatleft aut">
                                              <label>Người lớn                                                <span style="color: #fff;">
                                                 >= 12 tuổi
                                              </span>
                                              </label>
                                              <p class="quantity">
                                              <select name="adult" id="adult" >
                                                                                              <option value=1>1</option>
                                                                                              <option value=2>2</option>
                                                                                              <option value=3>3</option>
                                                                                              <option value=4>4</option>
                                                                                              <option value=5>5</option>
                                                                                              <option value=6>6</option>
                                                                                              <option value=7>7</option>
                                                                                              <option value=8>8</option>
                                                                                              <option value=9>9</option>
                                                                                              <option value=10>10</option>
                                                                                              <option value=11>11</option>
                                                                                              <option value=12>12</option>
                                                                                              <option value=13>13</option>
                                                                                              <option value=14>14</option>
                                                                                              <option value=15>15</option>
                                                                                              <option value=16>16</option>
                                                                                              <option value=17>17</option>
                                                                                              <option value=18>18</option>
                                                                                              <option value=19>19</option>
                                                                                              <option value=20>20</option>
                                                                                              <option value=21>21</option>
                                                                                              <option value=22>22</option>
                                                                                              <option value=23>23</option>
                                                                                              <option value=24>24</option>
                                                                                              <option value=25>25</option>
                                                                                              <option value=26>26</option>
                                                                                              <option value=27>27</option>
                                                                                              <option value=28>28</option>
                                                                                              <option value=29>29</option>
                                                                                              <option value=30>30</option>
                                                                                              <option value=31>31</option>
                                                                                              <option value=32>32</option>
                                                                                              <option value=33>33</option>
                                                                                              <option value=34>34</option>
                                                                                              <option value=35>35</option>
                                                                                              <option value=36>36</option>
                                                                                              <option value=37>37</option>
                                                                                              <option value=38>38</option>
                                                                                              <option value=39>39</option>
                                                                                              <option value=40>40</option>
                                                                                              <option value=41>41</option>
                                                                                              <option value=42>42</option>
                                                                                              <option value=43>43</option>
                                                                                              <option value=44>44</option>
                                                                                              <option value=45>45</option>
                                                                                              <option value=46>46</option>
                                                                                              <option value=47>47</option>
                                                                                              <option value=48>48</option>
                                                                                              <option value=49>49</option>
                                                                                              <option value=50>50</option>
                                                                                              <option value=51>51</option>
                                                                                              <option value=52>52</option>
                                                                                              <option value=53>53</option>
                                                                                              <option value=54>54</option>
                                                                                              <option value=55>55</option>
                                                                                              <option value=56>56</option>
                                                                                              <option value=57>57</option>
                                                                                              <option value=58>58</option>
                                                                                              <option value=59>59</option>
                                                                                              <option value=60>60</option>
                                                                                              <option value=61>61</option>
                                                                                              <option value=62>62</option>
                                                                                              <option value=63>63</option>
                                                                                              <option value=64>64</option>
                                                                                              <option value=65>65</option>
                                                                                              <option value=66>66</option>
                                                                                              <option value=67>67</option>
                                                                                              <option value=68>68</option>
                                                                                              <option value=69>69</option>
                                                                                              <option value=70>70</option>
                                                                                              <option value=71>71</option>
                                                                                              <option value=72>72</option>
                                                                                              <option value=73>73</option>
                                                                                              <option value=74>74</option>
                                                                                              <option value=75>75</option>
                                                                                              <option value=76>76</option>
                                                                                              <option value=77>77</option>
                                                                                              <option value=78>78</option>
                                                                                              <option value=79>79</option>
                                                                                              <option value=80>80</option>
                                                                                              <option value=81>81</option>
                                                                                              <option value=82>82</option>
                                                                                              <option value=83>83</option>
                                                                                              <option value=84>84</option>
                                                                                              <option value=85>85</option>
                                                                                              <option value=86>86</option>
                                                                                              <option value=87>87</option>
                                                                                              <option value=88>88</option>
                                                                                              <option value=89>89</option>
                                                                                              <option value=90>90</option>
                                                                                              <option value=91>91</option>
                                                                                              <option value=92>92</option>
                                                                                              <option value=93>93</option>
                                                                                              <option value=94>94</option>
                                                                                              <option value=95>95</option>
                                                                                              <option value=96>96</option>
                                                                                              <option value=97>97</option>
                                                                                              <option value=98>98</option>
                                                                                              <option value=99>99</option>
                                                                                              <option value=100>100</option>
                                                                                            </select>
                                               </p>  
                                              
                                        </div>
                                        <div class="floatleft chd">
                                            <label >Trẻ em                                              <span style="color: #fff;">
                                               2 -11 tuổi
                                            </span>     
                                            </label>
                                              <p class="quantity">
                                                    
                                          <select name="child" id="child">
                                                                                                  <option value="0">0</option>
                                                                                                  <option value="1">1</option>
                                                                                                  <option value="2">2</option>
                                                                                                  <option value="3">3</option>
                                                                                                  <option value="4">4</option>
                                                                                                  <option value="5">5</option>
                                                                                                  <option value="6">6</option>
                                                                                                  <option value="7">7</option>
                                                                                                  <option value="8">8</option>
                                                                                                  <option value="9">9</option>
                                                                                                  <option value="10">10</option>
                                                                                                  <option value="11">11</option>
                                                                                                  <option value="12">12</option>
                                                                                                  <option value="13">13</option>
                                                                                                  <option value="14">14</option>
                                                                                                  <option value="15">15</option>
                                                                                                  <option value="16">16</option>
                                                                                                  <option value="17">17</option>
                                                                                                  <option value="18">18</option>
                                                                                                  <option value="19">19</option>
                                                                                                  <option value="20">20</option>
                                                                                                  <option value="21">21</option>
                                                                                                  <option value="22">22</option>
                                                                                                  <option value="23">23</option>
                                                                                                  <option value="24">24</option>
                                                                                                  <option value="25">25</option>
                                                                                                  <option value="26">26</option>
                                                                                                  <option value="27">27</option>
                                                                                                  <option value="28">28</option>
                                                                                                  <option value="29">29</option>
                                                                                                  <option value="30">30</option>
                                                                                                  <option value="31">31</option>
                                                                                                  <option value="32">32</option>
                                                                                                  <option value="33">33</option>
                                                                                                  <option value="34">34</option>
                                                                                                  <option value="35">35</option>
                                                                                                  <option value="36">36</option>
                                                                                                  <option value="37">37</option>
                                                                                                  <option value="38">38</option>
                                                                                                  <option value="39">39</option>
                                                                                                  <option value="40">40</option>
                                                                                                  <option value="41">41</option>
                                                                                                  <option value="42">42</option>
                                                                                                  <option value="43">43</option>
                                                                                                  <option value="44">44</option>
                                                                                                  <option value="45">45</option>
                                                                                                  <option value="46">46</option>
                                                                                                  <option value="47">47</option>
                                                                                                  <option value="48">48</option>
                                                                                                  <option value="49">49</option>
                                                                                                  <option value="50">50</option>
                                                                                                  <option value="51">51</option>
                                                                                                  <option value="52">52</option>
                                                                                                  <option value="53">53</option>
                                                                                                  <option value="54">54</option>
                                                                                                  <option value="55">55</option>
                                                                                                  <option value="56">56</option>
                                                                                                  <option value="57">57</option>
                                                                                                  <option value="58">58</option>
                                                                                                  <option value="59">59</option>
                                                                                                  <option value="60">60</option>
                                                                                                  <option value="61">61</option>
                                                                                                  <option value="62">62</option>
                                                                                                  <option value="63">63</option>
                                                                                                  <option value="64">64</option>
                                                                                                  <option value="65">65</option>
                                                                                                  <option value="66">66</option>
                                                                                                  <option value="67">67</option>
                                                                                                  <option value="68">68</option>
                                                                                                  <option value="69">69</option>
                                                                                                  <option value="70">70</option>
                                                                                                  <option value="71">71</option>
                                                                                                  <option value="72">72</option>
                                                                                                  <option value="73">73</option>
                                                                                                  <option value="74">74</option>
                                                                                                  <option value="75">75</option>
                                                                                                  <option value="76">76</option>
                                                                                                  <option value="77">77</option>
                                                                                                  <option value="78">78</option>
                                                                                                  <option value="79">79</option>
                                                                                                  <option value="80">80</option>
                                                                                                  <option value="81">81</option>
                                                                                                  <option value="82">82</option>
                                                                                                  <option value="83">83</option>
                                                                                                  <option value="84">84</option>
                                                                                                  <option value="85">85</option>
                                                                                                  <option value="86">86</option>
                                                                                                  <option value="87">87</option>
                                                                                                  <option value="88">88</option>
                                                                                                  <option value="89">89</option>
                                                                                                  <option value="90">90</option>
                                                                                                  <option value="91">91</option>
                                                                                                  <option value="92">92</option>
                                                                                                  <option value="93">93</option>
                                                                                                  <option value="94">94</option>
                                                                                                  <option value="95">95</option>
                                                                                                  <option value="96">96</option>
                                                                                                  <option value="97">97</option>
                                                                                                  <option value="98">98</option>
                                                                                                  <option value="99">99</option>
                                                                                                  <option value="100">100</option>
                                                                                        </select>
                                              </p>          
                                             
                                                
                                        </div>
                                        <div class="floatleft inf">        
                                                <label > Em bé <span style="color: #fff;"> < 24 tháng </span>
                                                </label>
                                        <p class="quantity">
                                                
                                          <select name="infant">
                                                                                                  <option value="0">0</option>
                                                                                                  <option value="1">1</option>
                                                                                                  <option value="2">2</option>
                                                                                                  <option value="3">3</option>
                                                                                                  <option value="4">4</option>
                                                                                                  <option value="5">5</option>
                                                                                                  <option value="6">6</option>
                                                                                                  <option value="7">7</option>
                                                                                                  <option value="8">8</option>
                                                                                                  <option value="9">9</option>
                                                                                                  <option value="10">10</option>
                                                                                                  <option value="11">11</option>
                                                                                                  <option value="12">12</option>
                                                                                                  <option value="13">13</option>
                                                                                                  <option value="14">14</option>
                                                                                                  <option value="15">15</option>
                                                                                                  <option value="16">16</option>
                                                                                                  <option value="17">17</option>
                                                                                                  <option value="18">18</option>
                                                                                                  <option value="19">19</option>
                                                                                                  <option value="20">20</option>
                                                                                                  <option value="21">21</option>
                                                                                                  <option value="22">22</option>
                                                                                                  <option value="23">23</option>
                                                                                                  <option value="24">24</option>
                                                                                                  <option value="25">25</option>
                                                                                                  <option value="26">26</option>
                                                                                                  <option value="27">27</option>
                                                                                                  <option value="28">28</option>
                                                                                                  <option value="29">29</option>
                                                                                                  <option value="30">30</option>
                                                                                                  <option value="31">31</option>
                                                                                                  <option value="32">32</option>
                                                                                                  <option value="33">33</option>
                                                                                                  <option value="34">34</option>
                                                                                                  <option value="35">35</option>
                                                                                                  <option value="36">36</option>
                                                                                                  <option value="37">37</option>
                                                                                                  <option value="38">38</option>
                                                                                                  <option value="39">39</option>
                                                                                                  <option value="40">40</option>
                                                                                                  <option value="41">41</option>
                                                                                                  <option value="42">42</option>
                                                                                                  <option value="43">43</option>
                                                                                                  <option value="44">44</option>
                                                                                                  <option value="45">45</option>
                                                                                                  <option value="46">46</option>
                                                                                                  <option value="47">47</option>
                                                                                                  <option value="48">48</option>
                                                                                                  <option value="49">49</option>
                                                                                                  <option value="50">50</option>
                                                                                       </select> 
                                        </p>          
                                                   
                                              
                                        </div>
                                 </div>   
                                 <div class="content">
                                        <div class="butonsearch">
                                            <input type="submit"  id="cmdsearch" name="cmdsearch" value="Tìm chuyến bay"/>       
                                        </div> 
                                 </div>
                            <input type="hidden" name="price" id="price" value="" />
                            <input type="hidden" name="dt" id="dt" value="" />
                            <input  type="hidden" name="qt" value="0"/>
                             <input type="hidden" id="departCity_hidden1" name="dep" value="" />
                            <input type="hidden" id="returnCity_hidden1" name="des" value="" />
                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>
                        </div>     
                 </div>
                </form>
       </div><!--end tab-search-->
    </div>            
 </div>
</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
$(document).ready(function () { 
var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
var myDown = isIOS ? "touchstart" : "mousedown";
var myUp = isIOS ? "touchend" : "mouseup"; 
if(isIOS){
	$("#departCity").hover(function(e){
	 $(this).click();
  }); 
  $("#returnCity").hover(function(e){
    $(this).click();
  }); 
  $('.sanbay li.aircode').hover(function(){
  	$(this).click();
  });
  $('.sanbayres li.aircode').hover(function(){
    $(this).click();
  });
}
    $("#departCity").bind('click',function(e){ 
    var x = jQuery(this).position().left + jQuery(this).outerWidth();
    var y = jQuery(this).position().top - jQuery(document).scrollTop();
    //jQuery("#dialog").dialog('option', 'position', [x,y]);
       $chuoi = $(this).attr('data');
       var deOffset = $(this).offset(),elm="";
       //alert(deOffset.left + '='+deOffset.right);
      var inputHeight = $(this).height() +  $(window).scrollTop();
      $(".sanbay").dialog({
        closeOnEscape: true,
        autoOpen: false,
        title: 'Lựa chọn thành phố hoặc sân bay đi',
        closeOnEscape: true,
        draggable: false,
        resizable: false,
        autoOpen: false,
        width: 580,            
        position:[deOffset.left, deOffset.top +inputHeight]
      });

      $(".sanbayres").dialog({
          closeOnEscape: true,
	        autoOpen: true,
	        title: 'Lựa chọn thành phố hoặc sân bay đến',
	        closeOnEscape: true,
	        draggable: false,
	        resizable: false,
	        autoOpen: false,
	        width: 580,                           
	        position: [deOffset.left, deOffset.top +inputHeight]
      }).prev(".ui-dialog-titlebar").css("background","#097C9B !important");
    $('.sanbay').dialog({
              autoOpen: false,
              modal: false         
     });
 
}); 

   //diem den
   $("#returnCity").bind("click", function(event) {    
    var x = jQuery(this).position().left + jQuery(this).outerWidth();
    var y = jQuery(this).position().top - jQuery(document).scrollTop();
    //jQuery("#dialog").dialog('option', 'position', [x,y]);
    $chuoi = $(this).attr('data');
    var deOffset = $(this).offset(),elm="";
       //alert(deOffset.left + '='+deOffset.right);
    var inputHeight = $(this).height() +  $(window).scrollTop();
    
    $(".sanbayres").dialog({
        closeOnEscape: true,
        autoOpen: false,
        title: 'Lựa chọn thành phố hoặc sân bay đến',
        closeOnEscape: true,
        draggable: false,
        resizable: false,
        autoOpen: false,
        width: 580,                           
        position: [deOffset.left, deOffset.top +inputHeight]
    }).prev(".ui-dialog-titlebar").css("background","#097C9B !important");
    
   });
$("#departCity,#returnCity").click(function(){  
    if($(this).attr('id')=="departCity"){ 
     $('.sanbay').dialog("open");
     $( "#airport" ).click(function(){
            $(this).val("");
        });
     $('.sanbay li.aircode').click(function(){

         $("#departCity").val($(this).text());
	        if($(this).children('a').attr("airportcode")!== undefined){

	            $("#departCity_hidden").val($(this).children('a').attr("airportcode"));
	            $("#departCity_hidden1").val($(this).children('a').attr("code"));
	            $("#typeflight").val($(this).children('a').attr("typeflight"));
	       }else{
	            $("#departCity_hidden1").val($(this).attr("code"));
	            $("#departCity_hidden").val($(this).attr("code"));
	            $("#typeflight").val($(this).attr("typeflight"));
	       }
	       $(".sanbay").dialog("close");
     });
      $("#airport").autocomplete({
         source: function( request, response ) {
                url = "autocomple-air&keyword=" + request.term;
                $.getJSON(url, function(data) {
                   response($.map(data, function (item) {
                                    return {
                                    label: item.label ,
                                    country: item.country,
                                    value: item.value,
                                    }
                                }));
                }); 
            } ,
            open: function(event, ui) { 
            $(this).autocomplete("widget").css({
                "min-width": 250
            });
        },
        minLength: 2,
        select: function( event, ui ) {
                $("#departCity").val(ui.item.label);
                    $("#departCity_hidden").val(ui.item.value);
                    $("#departCity_hidden1").val(ui.item.value); 
                 $(".sanbay").dialog("close");
            },
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
        var inner_html = '<a>' + item.label + '<b style="float:right;color:#333">' + item.country + '</b></a>';
        return $( "<li class='aircode' typeflight="+item.country+"></li>" )
            .data( "item.autocomplete", item )
            .append(inner_html)
            .appendTo( ul );
    };  
    }else{
      $('.sanbayres').dialog("open"); 
    }
    $('.sanbayres li.aircode').click(function(){
       $("#returnCity").val($(this).text());
       if($(this).children('a').attr("airportcode")!== undefined){
         $("#returnCity_hidden").val($(this).children('a').attr("airportcode"));
         $("#returnCity_hidden1").val($(this).children('a').attr("code"));
         $("#typeflight").val($(this).children('a').attr("typeflight"));
        }else{
            $("#returnCity_hidden").val($(this).attr("code"));
            $("#returnCity_hidden1").val($(this).attr("code"));
            $("#typeflight").val($(this).attr("typeflight"));
        } 
        $(".sanbayres").dialog("close");
        return true;
    });
    $( "#airportres" ).click(function(){
            $(this).val("");
        });
        $("#airportres").autocomplete({
        source: function( request, response ) {
                url = "autocomple-air&keyword=" + request.term;
                $.getJSON(url, function(data) {
                   response($.map(data, function (item) { 
                                    return {
                                    label: item.label ,
                                    country: item.country,
                                    value: item.value,
                                    }
                                })); 
                });
            } ,
            open: function(event, ui) {
            $(this).autocomplete("widget").css({
                "width": 250
            });
        },
        minLength: 2,
        select: function( event, ui ) {
                $("#returnCity").val(ui.item.label);
                $("#returnCity_hidden").val(ui.item.value);
                $("#returnCity_hidden1").val(ui.item.value);
                 $(".sanbayres").dialog("close");
                return false;
            },
    })
    
    .data( "autocomplete" )._renderItem = function( ul, item ) {
        var inner_html = '<a>' + item.label + '<b style="float:right;color:#333">' + item.country + '</b></a>';
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append(inner_html)
            .appendTo( ul );
    };
    return false;
        });
    });    
</script>
<script type="text/javascript"> 
$(document).ready(function(){
    $('.tab-search').click(function(){  
        $("#tab-search").show();
        $("#tab-search .first").show(); 
        $("a.tab-search").addClass("selected"); 
    }); 
    
    $("#cmdsearch").click(function(){
        var adult = $("#adult").val();
        var child = $("#child").val();
                var dep = $("#departCity_hidden1").val();
        var des = $("#returnCity_hidden1").val();
          if(dep==""){
            alert('Điểm đi là bắt buộc !');
            $("#departCity").val('');
            $("#departCity").focus();
            //$('.sanbay').dialog("open");
            return false;
        }
        if(des==""){
            alert('Điểm đến là bắt buộc !');
            $("#returnCity").val('');
            $("#returnCity").focus();
           // $('.sanbayres').dialog("open");
            return false;
        }
        if(adult==""){//;kiem tra choiu vao rong hay khong
            alert('Số người lớn là bắt buộc !');
            $("#adult").val('');
            $("#adult").focus();
            return false;
        }else{
            if(isNaN(adult)==true){
                alert('Người lớn phải là số !');
                $("#adult").val('');
                $("#adult").focus();
                return false;
            }else{
                if(parseInt(adult) <= 0 || parseInt(adult) > 50){
                    alert('Số lượng người lớn nằm trong khoảng 1 -> 50 ');
                    $("#adult").val('');
                    $("#adult").focus();
                    return false;
                }
            }
        }
        
        if(child==""){//;kiem tra choiu vao rong hay khong
            alert('Số lượng trẻ em là bắt buộc !');
            $("#child").val('');
            $("#child").focus();
            return false;
        }else{
            if(isNaN(child)==true){
                alert('Trẻ em phải là số !');
                $("#child").val('');
                $("#child").focus();
                return false;
            }else{
                if(parseInt(child) < 0 || parseInt(child) > 49){
                    alert('Số lượng người trẻ em nằm trong khoảng 1 -> 49 ');
                    $("#child").val('');
                    $("#child").focus();
                    return false;
                }
            }
        }
        if($("#departCity_hidden1").val() == $("#returnCity_hidden1").val()){
            alert("Hành trình bị trùng");
            return false;
        }
       // $("#frmsearchair").submit();
        
    });  
}); 
</script>
   <div id="container"><div id="content_home">
 <div id="main_content"><div id="content">     <div class="box-support-sale">
    <div class="title_pricesale">
          <div class="sales-bar__title">Hotline: 0947899993 - 0947488880 - 0969388800 - 0947966660</div>
     </div>
</div>
     <div class="slide_module">
<script type="text/javascript" src="{{asset('res/js/js1/jquery.nivo.slider.pack.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('res/css/slideshow_tour.css')}}" />
  <div id="slideshow0" class="nivoSlider" style="width: auto; height: 400px;">
                               <a href="/"><img src="https://jetstarair.vn/image/data/ve-may-bay-tet-jetstarair.jpg" alt="Vé máy bay tết 2019" /></a>
                                           <a href="/"><img src="https://jetstarair.vn/image/data/slideshow/jet10.jpg" alt="Vé máy bay khuyến mãi" /></a>
                </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
    $('#slideshow0').nivoSlider();
});
--></script>
     <div class="box-order-content">
  <div class="box-content">
  <h3>
    <span><span>Vé máy bay khách đặt mới nhất</span></span>
  </h3>  
    <div class="box-order-cat">
      <ul>
                      <li attr="3531">
               <span class="title">2 vé máy bay <strong> Hồ Chí Minh (SGN)  - Hà Nội (HAN)</strong>
                                ngày <strong>2018-10-15</strong></span>

                        <p>
                            <img class="logo-airplane" src="https://jetstarair.vn/image/air/JS.png" />
                            <span class="time">38 Phút trước</span>
                            giá chỉ <span class="red bold">2,020,000 đ</span>/vé
                    <a class="viewDetail_link" attr="3531">
                         <span>Đặt ngay</span>
                     </a>
                        </p> 
                  <form name="frmsearchair" id="searchorder3531"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">

                         <input name="direction" id="loaive_r"  value="0" type="hidden" />

                         <input type="hidden"  name="depinput" value="Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="desinput" value="Hà Nội (HAN)" />

                         <input type="hidden"  name="departCity_hidden" value="8785,Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="returnCity_hidden" value="8772,Hà Nội (HAN)" />

                         <input type="hidden"  name="qt" value="0"/>

                         <input type="hidden"  name="dep" value="SGN" />

                         <input type="hidden"  name="des" value="HAN" />

                         <input type="hidden" name="depdate" value="15-10-2018" />

                         <input type="hidden" name="resdate" value="15-10-2018" />

                         <input type="hidden" name="adult" value="2" />

                         <input type="hidden" name="child" value="0" />
                         <input type="hidden" name="price" value="" />
                            <input type="hidden" name="dt" value="" />

                         <input type="hidden" name="infant" value="0" />                         

                         <input type="hidden" name="typeflight" id="typeflight" value="0"/>                         

                     </form>
             </li>
                      <li attr="3530">
               <span class="title">1 vé máy bay <strong> Đà Nẵng (DAD)  - Hà Nội (HAN)</strong>
                                ngày <strong>2018-10-18</strong></span>

                        <p>
                            <img class="logo-airplane" src="https://jetstarair.vn/image/air/JS.png" />
                            <span class="time">2 Phút trước</span>
                            giá chỉ <span class="red bold">380,000 đ</span>/vé
                    <a class="viewDetail_link" attr="3530">
                         <span>Đặt ngay</span>
                     </a>
                        </p> 
                  <form name="frmsearchair" id="searchorder3530"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">

                         <input name="direction" id="loaive_r"  value="0" type="hidden" />

                         <input type="hidden"  name="depinput" value="Đà Nẵng (DAD)" />

                         <input type="hidden"  name="desinput" value="Hà Nội (HAN)" />

                         <input type="hidden"  name="departCity_hidden" value="8761,Đà Nẵng (DAD)" />

                         <input type="hidden"  name="returnCity_hidden" value="8772,Hà Nội (HAN)" />

                         <input type="hidden"  name="qt" value="0"/>

                         <input type="hidden"  name="dep" value="DAD" />

                         <input type="hidden"  name="des" value="HAN" />

                         <input type="hidden" name="depdate" value="18-10-2018" />

                         <input type="hidden" name="resdate" value="18-10-2018" />

                         <input type="hidden" name="adult" value="1" />

                         <input type="hidden" name="child" value="0" />
                         <input type="hidden" name="price" value="" />
                            <input type="hidden" name="dt" value="" />

                         <input type="hidden" name="infant" value="0" />                         

                         <input type="hidden" name="typeflight" id="typeflight" value="0"/>                         

                     </form>
             </li>
                      <li attr="3529">
               <span class="title">3 vé máy bay <strong> Đà Nẵng (DAD)  - Hồ Chí Minh (SGN)</strong>
                                ngày <strong>2018-12-14</strong></span>

                        <p>
                            <img class="logo-airplane" src="https://jetstarair.vn/image/air/VJ.png" />
                            <span class="time">5 Phút trước</span>
                            giá chỉ <span class="red bold">660,000 đ</span>/vé
                    <a class="viewDetail_link" attr="3529">
                         <span>Đặt ngay</span>
                     </a>
                        </p> 
                  <form name="frmsearchair" id="searchorder3529"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">

                         <input name="direction" id="loaive_r"  value="0" type="hidden" />

                         <input type="hidden"  name="depinput" value="Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="desinput" value="Đà Nẵng (DAD)" />

                         <input type="hidden"  name="departCity_hidden" value="8785,Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="returnCity_hidden" value="8761,Đà Nẵng (DAD)" />

                         <input type="hidden"  name="qt" value="0"/>

                         <input type="hidden"  name="dep" value="SGN" />

                         <input type="hidden"  name="des" value="DAD" />

                         <input type="hidden" name="depdate" value="14-12-2018" />

                         <input type="hidden" name="resdate" value="16-12-2018" />

                         <input type="hidden" name="adult" value="3" />

                         <input type="hidden" name="child" value="0" />
                         <input type="hidden" name="price" value="" />
                            <input type="hidden" name="dt" value="" />

                         <input type="hidden" name="infant" value="0" />                         

                         <input type="hidden" name="typeflight" id="typeflight" value="0"/>                         

                     </form>
             </li>
                      <li attr="3529">
               <span class="title">3 vé máy bay <strong> Hồ Chí Minh (SGN)  - Đà Nẵng (DAD)</strong>
                                ngày <strong>2018-12-14</strong></span>

                        <p>
                            <img class="logo-airplane" src="https://jetstarair.vn/image/air/JS.png" />
                            <span class="time">5 Phút trước</span>
                            giá chỉ <span class="red bold">550,000 đ</span>/vé
                    <a class="viewDetail_link" attr="3529">
                         <span>Đặt ngay</span>
                     </a>
                        </p> 
                  <form name="frmsearchair" id="searchorder3529"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">

                         <input name="direction" id="loaive_r"  value="0" type="hidden" />

                         <input type="hidden"  name="depinput" value="Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="desinput" value="Đà Nẵng (DAD)" />

                         <input type="hidden"  name="departCity_hidden" value="8785,Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="returnCity_hidden" value="8761,Đà Nẵng (DAD)" />

                         <input type="hidden"  name="qt" value="0"/>

                         <input type="hidden"  name="dep" value="SGN" />

                         <input type="hidden"  name="des" value="DAD" />

                         <input type="hidden" name="depdate" value="14-12-2018" />

                         <input type="hidden" name="resdate" value="16-12-2018" />

                         <input type="hidden" name="adult" value="3" />

                         <input type="hidden" name="child" value="0" />
                         <input type="hidden" name="price" value="" />
                            <input type="hidden" name="dt" value="" />

                         <input type="hidden" name="infant" value="0" />                         

                         <input type="hidden" name="typeflight" id="typeflight" value="0"/>                         

                     </form>
             </li>
                      <li attr="3528">
               <span class="title">2 vé máy bay <strong> Tuy Hòa (TBB)  - Hồ Chí Minh (SGN)</strong>
                                ngày <strong>2018-10-30</strong></span>

                        <p>
                            <img class="logo-airplane" src="https://jetstarair.vn/image/air/JS.png" />
                            <span class="time">5 Phút trước</span>
                            giá chỉ <span class="red bold">190,000 đ</span>/vé
                    <a class="viewDetail_link" attr="3528">
                         <span>Đặt ngay</span>
                     </a>
                        </p> 
                  <form name="frmsearchair" id="searchorder3528"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">

                         <input name="direction" id="loaive_r"  value="0" type="hidden" />

                         <input type="hidden"  name="depinput" value="Tuy Hòa (TBB)" />

                         <input type="hidden"  name="desinput" value="Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="departCity_hidden" value="8763,Tuy Hòa (TBB)" />

                         <input type="hidden"  name="returnCity_hidden" value="8785,Hồ Chí Minh (SGN)" />

                         <input type="hidden"  name="qt" value="0"/>

                         <input type="hidden"  name="dep" value="TBB" />

                         <input type="hidden"  name="des" value="SGN" />

                         <input type="hidden" name="depdate" value="30-10-2018" />

                         <input type="hidden" name="resdate" value="30-11-2018" />

                         <input type="hidden" name="adult" value="2" />

                         <input type="hidden" name="child" value="0" />
                         <input type="hidden" name="price" value="" />
                            <input type="hidden" name="dt" value="" />

                         <input type="hidden" name="infant" value="0" />                         

                         <input type="hidden" name="typeflight" id="typeflight" value="0"/>                         

                     </form>
             </li>
               </ul>
    </div>
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
$(document).ready(function(){
    
   $('.box-order-cat ul li:last').addClass("end");
    
});
</script>
<script type="text/javascript">
  $(document).ready(function(){

     $("a.viewDetail_link").live('click',function(){
              var order_id = $(this).attr('attr');
             $('#searchorder'+order_id).submit(); 
     });
  });
</script>     <div class="box-flight-price">
 <div class="box-content">
     <div class="title_pricesale">
          <div class="sales-bar__title_s">Vé mở bán đến khi chương trình khuyến mại kết thúc, chương trình cũng có thể kết thúc sớm khi vé đã bán hết. Số lượng vé có hạn, áp dụng cho một số chuyến bay và ngày bay. Có điều kiện áp dụng.</div>
     </div>
  <div class="box-flight-content">
   <div id="flight_list"> 
      <div id="list_page_view">        
        <ul>
                                                                <li title="Hành trình từ Hồ chí minh(SGN) đến Quy Nhơn (UIH) Vào ngày 28-04-2017" id="view_detail_search" class="row1" stt="1">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">28-04-2017</p>
                        <p class="sale__orig">Quy Nhơn (UIH)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            90.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder1"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Quy Nhơn (UIH)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Quy Nhơn (UIH)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="UIH" />

                             <input type="hidden" name="depdate" value="28-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                                          <li title="Hành trình từ Hồ chí minh(SGN) đến Ban Mê Thuột (BMV) Vào ngày 24-04-2017" id="view_detail_search" class="row2" stt="2">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">24-04-2017</p>
                        <p class="sale__orig">Ban Mê Thuột (BMV)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            90.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder2"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Ban Mê Thuột (BMV)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Ban Mê Thuột (BMV)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="BMV" />

                             <input type="hidden" name="depdate" value="24-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                                          <li title="Hành trình từ Hồ chí minh(SGN) đến Ðà Lạt (DLI) Vào ngày 23-04-2017" id="view_detail_search" class="row3" stt="3">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">23-04-2017</p>
                        <p class="sale__orig">Ðà Lạt (DLI)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            90.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder3"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Ðà Lạt (DLI)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Ðà Lạt (DLI)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="DLI" />

                             <input type="hidden" name="depdate" value="23-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                                          <li title="Hành trình từ Hồ chí minh(SGN) đến Nha Trang (NHA) Vào ngày 23-04-2017" id="view_detail_search" class="row4" stt="4">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">23-04-2017</p>
                        <p class="sale__orig">Nha Trang (NHA)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            90.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder4"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Nha Trang (NHA)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Nha Trang (NHA)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="NHA" />

                             <input type="hidden" name="depdate" value="23-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                                          <li title="Hành trình từ Hồ chí minh(SGN) đến Phú Quốc (PQC) Vào ngày 12-04-2017" id="view_detail_search" class="row5" stt="5">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">12-04-2017</p>
                        <p class="sale__orig">Phú Quốc (PQC)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            90.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder5"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Phú Quốc (PQC)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Phú Quốc (PQC)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="PQC" />

                             <input type="hidden" name="depdate" value="12-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                                          <li title="Hành trình từ Hồ chí minh(SGN) đến Pleiku (PXU) Vào ngày 01-04-2017" id="view_detail_search" class="row6" stt="6">
                 <div class="sale__wrapper clearfix">
                    <div class="sale__details" style="min-height: 66px;">
                        <h3 class="sale__dest">Hồ chí minh(SGN)</h3>
                        <p class="sale__period">01-04-2017</p>
                        <p class="sale__orig">Pleiku (PXU)</p>
                    </div>
                    <div class="sale__price-wrap">
                        <span class="sale__price-disclaimer"><span class="deal__price-disclaimer--main">Từ</span> </span>
                        <p class="sale__price">
                            99.000<span class="currency">VND</span><sup>^</sup>
                        </p>
                    </div>
                </div>
                        <form name="frmsearchair" id="searchorder6"  action="https://jetstarair.vn/tim-ve-truc-tuyen?sessionid=8c94a02d89fb17d37ea6041a6a30e16a" method="POST">
                              <input name="direction" id="loaive_r"  value="0" type="hidden" />

                             <input type="hidden"  name="depinput" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="desinput" value="Pleiku (PXU)" />

                             <input type="hidden"  name="departCity_hidden" value="Hồ chí minh(SGN)" />

                             <input type="hidden"  name="returnCity_hidden" value="Pleiku (PXU)" />

                             <input type="hidden"  name="qt" value="0"/>

                             <input type="hidden"  name="dep" value="SGN" />

                             <input type="hidden"  name="des" value="PXU" />

                             <input type="hidden" name="depdate" value="01-04-2017" />

                             <input type="hidden" name="resdate" value="" />

                             <input type="hidden" name="adult" value="1" />

                             <input type="hidden" name="child" value="0" />
                             <input type="hidden" name="price" value="" />
                                <input type="hidden" name="dt" value="" />

                             <input type="hidden" name="infant" value="0" />                         

                             <input type="hidden" name="typeflight" id="typeflight" value="0"/>  
                        </form>
               </li>
                           </ul>
    </div> 
  </div>
  

  
 </div> 
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

     $("#view_detail_search").live('click',function(){
              var order_id = $(this).attr('stt');
             $('#searchorder'+order_id).submit(); 
     });
  });
</script>
                         <div class="box-content-service">
                        <ul>
                                                                        <li>
                                         <a href="https://jetstarair.vn/tin-khuyen-mai/ba-tieng-moi-ngay-thoa-uoc-mo-bay-voi-jetstar-pacific-gia-chi-tu-11-000-dong.html" title="&quot;Ba tiếng mỗi ngày - Thỏa ước mơ bay&quot; với Jetstar Pacific giá chỉ từ 11.000 đồng">
                                            <div class="news_left">
                                               <img src="https://jetstarair.vn/image/data/jetstar-3-tieng-moi-ngay-thoa-uoc-mo-bay.jpg" alt="&quot;Ba tiếng mỗi ngày - Thỏa ước mơ bay&quot; với Jetstar Pacific giá chỉ từ 11.000 đồng" />
                                            </div>
                                            <div class="news_right">
                                                <p class="promo-tile__title">&quot;Ba tiếng mỗi ngày - Thỏa ước mơ bay&quot; với Jetstar Pacific giá chỉ từ 11.000 đồng</p>
                                                <p class="promo-tile__subtitle">Hãng hàng không Jetstar Pacific tiếp tục tung ra chương trình khuyến mại "Ba tiếng mỗi ngày - Th...</p>
                                            </div>
                                            <p class="promo-tile__cta">
                                                <span class="promo-tile__cta-container">
                                                    Xem chi tiết
                                                </span>
                                                <i class="promo-tile__cta-icon">></i>
                                            </p>
                                          </a>  
                                        </li>
                                                                                <li>
                                         <a href="https://jetstarair.vn/tin-khuyen-mai/cuoi-tuan-sieu-khuyen-mai-cung-ve-may-bay-jetstar.html" title="Cuối tuần siêu khuyến mại cùng vé máy bay Jetstar">
                                            <div class="news_left">
                                               <img src="https://jetstarair.vn/image/data/jetstar/hp_specialWFF_vn.jpg" alt="Cuối tuần siêu khuyến mại cùng vé máy bay Jetstar" />
                                            </div>
                                            <div class="news_right">
                                                <p class="promo-tile__title">Cuối tuần siêu khuyến mại cùng vé máy bay Jetstar</p>
                                                <p class="promo-tile__subtitle">Chương trình khuyến mãi cuối tuần của Jetstar bắt đầu từ 11:00 ngày Thứ Sáu và kết thúc vào 23:59...</p>
                                            </div>
                                            <p class="promo-tile__cta">
                                                <span class="promo-tile__cta-container">
                                                    Xem chi tiết
                                                </span>
                                                <i class="promo-tile__cta-icon">></i>
                                            </p>
                                          </a>  
                                        </li>
                                                                                <li>
                                         <a href="https://jetstarair.vn/tin-khuyen-mai/jetstar-tang-voucher-50-000-dong-khi-mua-ve-may-bay-truc-tuyen-cuoi-tuan.html" title="Jetstar tặng voucher 50.000 đồng khi mua vé máy bay trực tuyến cuối tuần">
                                            <div class="news_left">
                                               <img src="https://jetstarair.vn/image/data/jetstar-tang-voucher-50000-dong-khi-mua-ve-may-bay-truc-tuyen-cuoi-tuan.jpg" alt="Jetstar tặng voucher 50.000 đồng khi mua vé máy bay trực tuyến cuối tuần" />
                                            </div>
                                            <div class="news_right">
                                                <p class="promo-tile__title">Jetstar tặng voucher 50.000 đồng khi mua vé máy bay trực tuyến cuối tuần</p>
                                                <p class="promo-tile__subtitle">Từ ngày 3 – 30/11, Jetstar tặng voucher 50.000 đồng khi mua vé máy bay trực tuyến cuối tuần và thanh...</p>
                                            </div>
                                            <p class="promo-tile__cta">
                                                <span class="promo-tile__cta-container">
                                                    Xem chi tiết
                                                </span>
                                                <i class="promo-tile__cta-icon">></i>
                                            </p>
                                          </a>  
                                        </li>
                                                                </ul>
                    </div>
         <div id="maps">  
 <div class="hd_order">
    <div class="block-index">
            <h3>
               <span><span>Hướng dẫn đặt vé</span></span>
            </h3>
      <div class="block-content">
            <ul class="index-list">
                 <li>
                 <em class="fa fa-phone"></em>
                 <p class="list-head">Qua Điện thoại:</p>
                                                            <p>Vé Quốc Nội  <strong>0947488880</strong></p>
                                           <p>Vé Quốc Nội  <strong>0947899993</strong></p>
                                           <p>Vé Quốc Tế  <strong>0969388800</strong></p>
                                           <p>Vé Quốc Tế  <strong>0947966660</strong></p>
                       
                    
                 </li>
                 <li>
                 <em class="fa fa-globe"></em>
                 <p class="list-head">Qua website: <a href="https://jetstarair.vn/">https://jetstarair.vn/</a></p>
                 <p>Trực tiếp trên website, nhanh chóng - tiện lợi</p>
                 </li>
                 <li>
                 <em class="fa fa-map-marker"></em>
                 <p class="list-head">Đến trực tiếp văn phòng của chúng tôi:</p>
                 <p>Công Ty TNHH PN Việt Nam
8, Trần Quốc Tuấn, Phường 1, Quận Gò Vấp, Thành Phố Hồ Chí Minh, Việt Nam</p>
                 </li>
                 
            </ul>
       </div>
  </div>
</div> <!--end huong dan dat ve-->
<div class="hd_thanhtoan">
                    <h3>
                        <span><span>Hướng dẫn thanh toán</span></span>
                    </h3>
                    <div class="block-content">
                        <ul class="index-list">
                            <li>
                                <em class="fa fa-money"></em>
                                <p class="list-head">Thanh toán bằng tiền mặt tại văn phòng</p>
                                <p>Sau khi đặt vé thành công, Quý khách vui lòng qua văn phòng để thanh toán và nhận vé.</p>
                            </li>
                            <li>
                                <em class="fa fa-credit-card"></em>
                                <p class="list-head">Thanh toán qua cà thẻ (POS)</p>
                                <p>Quý khách có thể thanh toán bằng hình thức cà thẻ (POS) tại văn phòng công ty.</p>
                            </li>
                            <li>
                                <em class="fa fa-home"></em>
                                <p class="list-head">Thanh toán tại nhà</p>
                                <p>Nhân viên Phòng vé jetstarair.vn sẽ giao vé và thu tiền tại nhà theo địa chỉ Quý khách cung cấp.</p>
                            </li>
                            <li>
                                <em class="fa fa-random"></em>
                                <p class="list-head">Thanh toán qua Chuyển khoản</p>
                                <p>Quý khách có thể thanh toán cho chúng tôi bằng cách chuyển khoản trực tiếp tại ngân hàng, chuyển khoản qua ATM, hoặc qua Internet banking.</p>
                               
                            </li>
						</ul>
                    </div>         
</div>
</div>     
</div> 
<div class="clear"></div>
 <div class="box-seo-content">
    <h1>JetstarAir.VN - Vé máy bay Jetstar giá rẻ</h1>
  
  <h2>Đặt vé máy bay JetstarAir</h2>
  Đặt vé máy bay JetstarAir thật dễ dàng và đơn giản chỉ. Hỗ trợ săn vé <b>Jetstar Air</b> siêu khuyến mãi thật hấp dẫn, hệ thống tổng đài hỗ trợ hoàn toàn miễn phí luôn hân hạnh được phục vụ quý khách. 
  <br>Với hệ thống tìm kiếm chuyến bay theo thời gian thực mang đến cho quý khách những chiếc vé máy bay <b>JetstarAir</b> chính xác nhất, giá vé rẻ nhất và giờ bay hợp lý nhất.
<h2>Săn vé khuyến mãi JetstarAir</h2>
JetstarAir thường xuyên mở bán nhiều chương trình khuyến mãi hấp dẫn với giá vé siêu rẻ, mang đến cho quý khách hàng cơ hội trải nghiệm những chuyến đi tuyệt vời, những điểm đến thú vị.
<h2>Thanh toán vé JetstarAir</h2>
Chúng tôi hỗ trợ nhiều hình thức thanh toán vé Jetstar Air thật đơn giản và nhanh chóng, hỗ trợ thanh toán online hoặc tiền mặt an toàn, tiện dụng và bảo mật. 
 </div> 
</div></div></div></div>
@endsection