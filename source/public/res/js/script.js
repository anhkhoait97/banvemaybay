function IsEmail(e) {
    var t = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return t.test(e) ? !0 : !1
}

function IsPhone(e) {
    var t = /^([0-1])?[0-9]{3}[0-9]{3}[0-9]{4}$/;
    return t.test(e) ? !0 : !1
}

function close_fd(e) {
    $("#f_detail" + e).css("display", "none")
}

function ShowProgressAnimation() {
    $("#loading-div-background").show().fadeOut(17e3)
}

function HideProgressAnimation() {
    $("#loading-div-background").hide()
}

function disabledate(e) {
    0 == e ? ($("#loaive_r").removeAttr("checked"), $("#loaive_o").attr("checked", "checked"), $('#ngayve').append('<span class="disable_date"  onclick="disabledate(1);"></span>')) : ($("span.disable_date").remove(), $("#ngayve").css("display", "block"), $("#loaive_r").attr("checked", "checked"))
}
function checkformsearch() {
    return $("#loaive_r").attr("checked") && "" == $("#flight-checkout").val() ? (alert(" Vui lĂ²ng chá»n ngĂ y vá»"), $("#flight-checkout").focus(), !1) : "" == $("#departCity").val() ? (alert("Xin vui lĂ²ng chá»n Ä‘iá»ƒm Ä‘i!"), $("#departCity").focus(), !1) : "" == $("#returnCity").val() ? (alert("Xin vui lĂ²ng chá»n Ä‘iá»ƒm Ä‘áº¿n!"), $("#returnCity").focus(), !1) : !0
}

function addquantity(e, t) {
    var a = 9,
        i = parseInt($("#quantity-" + e).val()) + 1;
    $("#quantity-" + e).val(i), i > a && (i = 9), liveprice(i, t)
}

function clrquantity(e, t) {
    var a = parseInt($("#quantity-" + e).val()) - 1,
        i = parseInt($("#quantity-" + e + "-minimum").val());
    i > a && (a = i), $("#quantity-" + e).val(a), liveprice(a, t)
}

function liveprice(e, t) {
    $("input#" + t).val(e)
}

function addpage() {
    var e = $("#page-maxnimum").val(),
        t = parseInt($("#page").val()) + 1;
    t > e && (t = e), $("#page").val(t), pagination(t)
}

function clrpage() {
    var e = parseInt($("#page").val()) - 1;
    1 > e && (e = 1), $("#page").val(e), pagination(e)
}

function loadsubmit() {
    $(".ajax_loader").show(), $("body").css("overflow", "hidden")
}

function formatCurrency(e) {
    e = e.toString().replace(/\$|\,/g, "");
    var t = e;
    isNaN(e) && (e = "0"), sign = e == (e = Math.abs(e)), e = Math.floor(100 * e + .50000000001), cents = e % 100, e = Math.floor(e / 100).toString(), cents < 10 && (cents = "0" + cents);
    for (var a = 0; a < Math.floor((e.length - (1 + a)) / 3); a++) e = e.substring(0, e.length - (4 * a + 3)) + "." + e.substring(e.length - (4 * a + 3));
    return 3e4 > t ? (sign ? "" : "-") + e + " .USD" : (sign ? "" : "-") + e + " .VND"
}
$(document).ready(function() {
    $(".ajax_loader").hide(), $(".image_calendar").click(function() {
        $("#calendar_content").toggle("slow")
    }), $(".image_calendar_close").click(function() {
        $("#calendar_content").toggle("slow")
    }), $("div#sbquocte").hide(), $("a#sbquocnoi").click(function() {
        $(this).addClass("selected"), $("a#sbquocte").removeClass("selected"), $("div#sbquocte").hide(), $("div#sbquocnoi").show()
    }), $("a#sbquocte").on("click", function() {
        $(this).addClass("selected"), $("a#sbquocnoi").removeClass("selected"), $("div#sbquocte").show(), $("div#sbquocnoi").hide()
    }), $(".ajax_loader").hide(), $("#flights-checkin").datepicker({
        minDate: "0",
        numberOfMonths: 2,
        dateFormat: "dd-mm-yy",
        monthNames: ["THĂNG 1 -", "THĂNG 2 -", "THĂNG 3 -", "THĂNG 4 -", "THĂNG 5 -", "THĂNG 6 -", "THĂNG 7 -", "THĂNG 8 -", "THĂNG 9 -", "THĂNG 10 -", "THĂNG 11 -", "THĂNG 12 -"],
        dayNames: ["Chá»§ nháº­t", "Thá»© 2", "Thá»© 3", "Thá»© 4", "Thá»© 5", "Thá»© 6", "Thá»© 7"],
        dayNamesShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        defaultDate: "+1w",
        onSelect: function() {
            var e = $("#flights-checkin").datepicker("getDate");
            $("#flights-checkout").datepicker("option", "minDate", $(".depDate").datepicker("getDate")), e.setDate(e.getDate() + 3), $("#flights-checkout").datepicker("setDate", e)
        }
    }), $("#flights-checkin").change(function() {
        var e = $("#flights-checkin").datepicker("getDate");
        $("#flights-checkout").datepicker("option", "minDate", $(".depDate").datepicker("getDate")), e.setDate(e.getDate() + 2), $("#flights-checkout").datepicker("setDate", e)
    }), $("#flights-checkout").datepicker({
        minDate: 0,
        numberOfMonths: 2,
        dateFormat: "dd-mm-yy",
        monthNames: ["THĂNG 1 -", "THĂNG 2 -", "THĂNG 3 -", "THĂNG 4 -", "THĂNG 5 -", "THĂNG 6 -", "THĂNG 7 -", "THĂNG 8 -", "THĂNG 9 -", "THĂNG 10 -", "THĂNG 11 -", "THĂNG 12 -"],
        dayNames: ["Chá»§ Nháº­t", "Thá»© 2", "Thá»© 3", "Thá»© 4", "Thá»© 5", "Thá»© 6", "Thá»© 7"],
        dayNamesShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"]
    }), $("html").bind("click", function(e) {
        !$(".sanbay").dialog("isOpen") || $(e.target).is(".ui-dialog, a") || $(e.target).closest(".ui-dialog").length || $(".sanbay").dialog("close"), !$(".sanbayres").dialog("isOpen") || $(e.target).is(".ui-dialog, a") || $(e.target).closest(".ui-dialog").length || $(".sanbayres").dialog("close")
    }), $("#ngayve").css("position", "relative"), disabledate($("#loaive_o").attr("checked") ? 0 : 1), $("#frmsearchair").submit(function() {
        return $("#departCity_hidden1").val() == $("#returnCity_hidden1").val() ? (alert("HĂ nh trĂ¬nh bá»‹ trĂ¹ng"), !1) : $("input#loaive_r:checked").length > 0 && "" == $('input[name="flights-checkout"]').val() ? (alert("chÆ°a chá»n ngĂ y vá»"), $('input[name="flights-checkout"]').focus(), !1) : void 0
    }), $("input:radio[name=radbook]:checked").parent("tr").addClass("selected"), $("input").click(function() {
        $("input:radio[name=radbook]:not(:checked)").parent().parent().removeClass("selected"), $("input:radio[name=radbook]:checked").parent().parent().addClass("selected")
    }), $("input:radio[name=radbookout]:checked").parent("tr").addClass("selected"), $("input").click(function() {
        $("input:radio[name=radbookout]:not(:checked)").parent().parent().removeClass("selected"), $("input:radio[name=radbookout]:checked").parent().parent().addClass("selected")
    })
}), $(function() {
    $(".results tr").show(), $(".filters li input[type=checkbox]").click(function() {
        var e = $(this).val();
        if ("ALL" == e) $(this).is(":checked") ? ($(".results tr").removeClass("hidden"), $(".filters li input[type=checkbox]").attr("checked", "checked"), $(".filters li").addClass("checked")) : ($(".results tr").addClass("hidden"), $(".filters li input[type=checkbox]").removeAttr("checked"), $(".filters li").removeClass("checked"), $(".results thead tr").removeClass("hidden"));
        else if ($(this).is(":checked")) {
            $(".results tr." + e).removeClass("hidden");
            var t = "",
                a = $(".results tr." + e).attr("class");
            t = t + " " + a;
            var i = t.split(" ");
            $.each(i, function() {
                $(".filters input[value=" + this + "]").parent("li").addClass("checked")
            }), $(".filters input[value=" + this + "]").attr("checked", "checked")
        } else $(".results tr." + e).addClass("hidden"), $(".filters input:checked").length <= 0 && ($(".results tr." + e).addClass("hidden"), $(this).parent("li").removeClass("checked")), $(this).parent("li").removeClass("checked")
    }), $(".box-content-tram").show()
}), $(function() {
    var e = $("#txtname"),
        t = $("#email"),
        a = $("#txtaddress"),
        i = $("#txtphone"),
        n = $("#txtfullname");
    allFields = $([]).add(e).add(t).add(a).add(i).add(n), tips = $(".validateTips")
});